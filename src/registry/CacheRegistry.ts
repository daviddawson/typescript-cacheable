/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable unused-imports/no-unused-vars */
/* eslint-disable security/detect-object-injection */
import { CacheableOptions } from '../CacheableOptions'
import { ExpiringMap } from '../ExpiringMap'

export const cacheProperty = '__typescript_cacheable_cache__'
export type MethodMap = Map<string, ExpiringMap<string, unknown>>
export type ClassMap = Map<string, MethodMap>
export type Cache = Map<string, unknown>

export abstract class CacheRegistry {
    public getOrInit(target: unknown, methodName: string, options?: CacheableOptions): ExpiringMap<string, unknown> {
        this.processOptions(target, methodName, options)
        const cache = this.getOrInitCache(target, methodName)
        if (!cache.has(methodName)) {
            cache.set(methodName, new ExpiringMap<string, unknown>())
        }
        return <ExpiringMap<string, unknown>>cache.get(methodName)
    }

    public getOrInitCache(target: unknown, methodName?: string): Cache {
        //
        // Does the cache exist?
        //
        const cacheHost = this.getCacheHost(target)
        if (!Object.prototype.hasOwnProperty.call(cacheHost, cacheProperty)) {
            this.initCache(cacheHost)
        }
        return <Cache>cacheHost[cacheProperty]
    }

    public initCache(cacheHost?: unknown): void {
        if (cacheHost === undefined) {
            throw 'cacheHost must be defined'
        }
        Object.defineProperty(cacheHost, cacheProperty, <PropertyDescriptor>{
            configurable: true,
            enumerable: false,
            writable: false,
            value: this.newCache(),
        })
    }

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public initialiseStore(storeFunction: unknown): void {}

    protected getCacheHost(target: unknown, methodName?: string): unknown {
        return target
    }

    protected getMethodMap(cache: Cache, target: unknown, isLocal: boolean): MethodMap {
        if (isLocal) {
            const localCache = <ClassMap>cache
            const className = target.constructor.name
            if (!localCache.has(className)) {
                localCache.set(className, new Map<string, ExpiringMap<string, unknown>>())
            }
            return localCache.get(className)
        }
        return <MethodMap>cache
    }

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    protected processOptions(target: unknown, methodName: string, _options?: CacheableOptions): void {}

    protected abstract newCache(): Cache
}
